import base64
import colorsys
import cv2
import itertools
import math
import numpy as np
import os

from skimage import io, transform


# From https://stackoverflow.com/questions/63175828/convert-numpy-array-into-a-bytestring-readable-by-the-src-of-an-html-img-tag
def ndarray_to_b64(ndarray):
    img = cv2.cvtColor(ndarray, cv2.COLOR_RGB2BGR)
    _, buffer = cv2.imencode('.png', img)
    return base64.b64encode(buffer).decode('utf-8')


def handle_request(request):
    key = request.args.get('key')
    if key != 'PUT YOUR KEY HERE':
        return 'key missing'
    image_url = request.args.get('url')
    if not image_url:
        return 'url missing'
    block_count = int(request.args.get('count') or 1000)
    banned_colors = request.args.get('banned_colors') or ''
    banned_colors = set(banned_colors.split(','))
    print(list(banned_colors))

    image = io.imread(image_url)
    image = run_steps(shape_sampled(0, 0, 0, banned_colors), block_count, image)
    image_bytes = ndarray_to_b64(image)
    return f'<img style="image-rendering: pixelated; width: 50%; margin-left: 25%; margin-right: 25%;" src="data:image/png;base64,{image_bytes}">'


colors = {
    "Basic-Black": ((28, 26, 27)),
    "Basic-Gray": ((156, 157, 149)),
    "White": ((229, 231, 228)),
    "Basic-Red": ((192, 26, 38)),
    "Basic-Orange": ((237, 104, 47)),
    "Basic-Yellow": ((249, 209, 23)),
    "Basic-Green": ((0, 134, 71)),
    "Basic-Blue": ((0, 82, 148)),
    "Basic-Purple": ((59, 43, 114)),
    "Basic-Brown": ((93, 56, 47)),
    "Neon-Pink": ((254, 1, 110)),
    "Neon-Orange": ((254, 99, 71)),
    "Neon-Yellow": ((237, 235, 0)),
    "Neon-Green": ((0, 155, 46)),
    "Neon-Blue": ((0, 79, 187)),
    "Neon-Transparent": ((221, 216, 220)),
    "Pastel-Pink": ((234, 136, 175)),
    "Pastel-Yellow": ((247, 219, 93)),
    "Pastel-Green": ((197, 225, 141)),
    "Pastel-Blue": ((145, 200, 230)),
    "Pastel-Purple": ((152, 146, 208)),
    "Basic-Peach": ((229, 191, 170)),
    "Gold": ((154, 130, 66)),
    "Silver": ((128, 136, 138)),
    "Glow-in-the-Dark": ((225, 228, 209)),
    "Dark_Red": ((127, 20, 66)),
    "Lime_Green": ((126, 208, 82)),
    "Turquoise": ((78, 200, 224)),
    # Values below were manually obtained from Plus-Plus by email
    "Forest_Green": ((41, 75, 43)),
    "Navy_Blue": ((0, 44, 77)),
    "Dark_Gray": ((59, 60, 67)),
}

# Sample any missing color values from the center pixel of the corresponding Make-A-Mix image
for name, rgb in colors.items():
    if rgb is None:
        block_image = io.imread(f'https://cdn.shopify.com/s/files/1/0351/0344/0940/products/Make-A-Mix_Images_Individual-Pieces_{name}_600x.jpg')
        height, width, depth = block_image.shape
        coordinate = int(height / 2), int(width / 2)
        colors[name] = rgb = tuple(block_image[coordinate])
        # Print the color out so we can cache it in the array above if desired.
        print(f"\"{name}\": {rgb},")


def rgb2rgba(rgb):
    r, g, b, *_ = rgb
    return np.array([r, g, b, 255.0])


hsv_cache = {}


def rgb2hsv(rgb):
    r, g, b, *a = rgb
    rgb = (r, g, b)
    if rgb not in hsv_cache:
        hsv_cache[rgb] = colorsys.rgb_to_hsv(*rgb)
    return hsv_cache[rgb]


def hsv_distance(target, actual, h, s, v):
    h0, s0, v0 = rgb2hsv(target)
    h1, s1, v1 = rgb2hsv(actual)
    dh, ds, dv = h0 - h1, s0 - s1, (v0 - v1) % 180 / 180
    return dh * dh * h + ds * ds * s + dv * dv * v


def rgb_distance(target, actual, *args):
    r0, g0, b0, *a0 = target
    r1, g1, b1, *a1 = actual
    dr, dg, db = r0 - r1, g0 - g1, b0 - b1
    return math.sqrt(dr * dr + dg * dg + db * db)


def color_distance(target, actual, h, s, v):
    return rgb_distance(target, actual, h, s, v)


# Blocks come in limited colors. Find the closest block color to the provided color.
def closest_color(target_color, h, s, v, banned_colors):
    min_color = [255, 255, 255]
    min_distance = math.sqrt(3 * (255 ** 2))
    for actual_color_name, actual_color in colors.items():
        if actual_color_name in banned_colors:
            continue
        distance = color_distance(target_color, actual_color, h, s, v)
        if distance < min_distance:
            min_distance = distance
            min_color = actual_color
    return min_color


def add_color(left, right):
    r0, g0, b0, *_ = left
    r1, g1, b1, *_ = right
    return r0 + r1, g0 + g1, b0 + b1


def multiply_color(left, right):
    r0, g0, b0, *_ = left
    r1, g1, b1, *_ = right
    return r0 * r1, g0 * g1, b0 * b1


def divide_color(color, denominator):
    r, g, b, *_ = color
    return r / denominator, g / denominator, b / denominator


def multiply_point(point, product):
    x, y, *_ = point
    return x * product, y * product


sections = [
    (-1, -1), (1, -1),
    (-2, 0), (-1, 0), (0, 0), (1, 0), (2, 0),
    (-1, 1), (1, 1)
]
sections_per_block = len(sections)
sections_per_block_edge = int(math.sqrt(sections_per_block))


def get_block_sections(block_x, block_y):
    offset_x = block_x * sections_per_block_edge
    offset_y = block_y * sections_per_block_edge
    for (section_x, section_y) in sections:
        if (block_x + block_y) % 2 == 0:
            # Every other block is rotated 90 degrees around the origin
            section_x, section_y = section_y, section_x
        yield offset_x + section_x + 2, offset_y + section_y + 2


def input_to_plan(input_image, block_count, *_):
    height, width, _ = input_image.shape
    pixel_count = height * width
    ratio = math.sqrt(block_count / pixel_count)
    return (transform.rescale(input_image, (ratio, ratio, 1.0), anti_aliasing=True) * 255.0).astype(np.uint8)


def input_to_section(input_image, block_count, *_):
    height, width, *_ = input_image.shape
    pixel_count = height * width
    ratio = math.sqrt(sections_per_block * block_count / pixel_count)
    return (transform.rescale(input_image, (ratio, ratio, 1.0), anti_aliasing=True) * 255.0).astype(np.uint8)


def section_to_block(section_image, *_):
    width, height, *_ = section_image.shape
    block_width, block_height = int(width / sections_per_block_edge), int(height / sections_per_block_edge)
    block_image = np.zeros(section_image.shape)
    for block in itertools.product(range(block_width - 1), range(block_height - 1)):
        block_color = (0, 0, 0)
        block_sections = list(get_block_sections(*block))
        for section in block_sections:
            try:
                block_color = add_color(block_color, section_image[section])
            except IndexError:
                continue
        block_color = divide_color(block_color, len(block_sections))
        for section in block_sections:
            try:
                block_image[section][:3] = block_color
            except IndexError:
                continue

    return block_image.astype(np.uint8)


def block_to_palette(h, s, v, banned_colors):
    def block_to_palette(block_image, *_):
        width, height, *_ = block_image.shape
        block_width, block_height = int(width / sections_per_block_edge), int(height / sections_per_block_edge)
        palette_image = np.zeros((width, height, 4))
        for block in itertools.product(range(block_width - 1), range(block_height - 1)):
            block_color = closest_color(block_image[multiply_point(block, sections_per_block_edge)], h, s, v, banned_colors)
            for section in get_block_sections(*block):
                palette_image[section] = rgb2rgba(block_color)

        return palette_image.astype(np.uint8)

    return block_to_palette


def palette_to_plan(palette_image, *_):
    width, height, *_ = palette_image.shape
    block_width, block_height = int(width / sections_per_block_edge), int(height / sections_per_block_edge)
    plan_image = np.zeros((block_width, block_height, 4))
    for block in itertools.product(range(block_width - 1), range(block_height - 1)):
        plan_image[block] = rgb2rgba(palette_image[multiply_point(block, sections_per_block_edge)])

    return plan_image.astype(np.uint8)


def plan_to_block(plan_image, *_):
    block_width, block_height, *_ = plan_image.shape
    block_image = np.zeros((block_width * sections_per_block_edge, block_height * sections_per_block_edge, 4))
    for block in itertools.product(range(block_width - 1), range(block_height - 1)):
        for section in get_block_sections(*block):
            block_image[section] = rgb2rgba(plan_image[block])

    return block_image.astype(np.uint8)


def get_or_create_image(path, create, *create_args):
    dir_path = os.path.dirname(path)
    if not os.path.isdir(dir_path):
        os.makedirs(dir_path)

    if os.path.isfile(path) and not force_recreate:
        # The image has already been created and we're not forcing recreation; load it from disk.
        print(f'Loading {path} from disk')
        return io.imread(path)

    print(f'Generating {path}')
    image = create(*create_args)
    io.imsave(path, image)
    return image


def run_steps(steps, block_count, input_image):
    image = input_image
    for step in steps:
        image = step(image, block_count)
    return image


def shape_sampled(h, s, v, banned_colors):
    return [
        input_to_section,
        section_to_block,
        block_to_palette(h, s, v, banned_colors)
    ]